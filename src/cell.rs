use crate::result::{Error, Result};
use crate::utils::*;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

use std::fmt;

#[derive(Debug, Default, Clone)]
pub(crate) struct Cell {
    pub pos: Vec2,
    pub has_mine: bool,
    pub is_revealed: bool,
    pub is_press: bool,
    pub has_flag: bool,
    pub neighboor: u8,
    pub wall: &'static str,
    pub is_exploded: bool,
    pub wrong_flag: bool,
    pub implicit_flag: bool,
}

impl Cell {
    pub fn mirror(&mut self, oth: &Self) {
        self.has_flag = oth.has_flag;
        self.is_press = oth.is_press;
        self.pos = oth.pos.clone();
        self.wall = oth.wall;
    }
    pub fn toggle_flag(&mut self) -> i16 {
        if !self.is_revealed {
            self.is_press = false;
            self.has_flag = !self.has_flag;
            print!("{}", self);
            if self.has_flag {
                1
            } else {
                -1
            }
        } else {
            0
        }
    }
    pub fn check_win(&self) -> bool {
        self.is_revealed || self.has_mine
    }
    pub fn toggle_press(&mut self, val: bool) {
        if !self.is_revealed && !self.has_flag {
            self.is_press = val;
            print!("{}", self);
        }
    }
    pub fn final_reveal(&mut self) {
        self.is_press = false;
        if self.is_revealed || (self.has_flag && self.has_mine) {
            ()
        } else if self.has_flag {
            self.wrong_flag = true;
        } else if self.has_mine {
            self.is_revealed = true;
        }
    }
    pub fn final_reveal_win(&mut self) {
        self.is_press = false;
        if self.is_revealed || (self.has_flag && self.has_mine) {
            ()
        } else if self.has_flag {
            self.wrong_flag = true;
        } else if self.has_mine {
            self.implicit_flag = true;
            self.has_flag = true;
        }
    }
    pub fn reveal(&mut self) -> Result<bool> {
        if self.has_flag {
            return Ok(false);
        }

        self.is_revealed = true;
        self.is_press = false;
        print!("{}", self);
        if self.has_mine {
            self.is_exploded = true;
            Err(Error::BombExplode)
        } else {
            Ok(self.neighboor == 0)
        }
    }
}

use crate::settings::{BLANK_COLOR, FILLED_COLOR};
pub const HOVER_COLOR: color::AnsiValue = color::AnsiValue(6);
use termion::color;
const COLOR_FLAG: color::AnsiValue = color::AnsiValue(1);
pub(crate) const FLAG: &str = "⚑";
const BOMB: &str = "¤";
impl fmt::Display for Cell {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut dsp: String = String::new();
        if self.has_flag {
            if self.wrong_flag {
                dsp += &color::AnsiValue(0).fg_string();
                dsp += &color::AnsiValue(1).bg_string();
            } else if self.implicit_flag {
                dsp += &color::AnsiValue(4).fg_string();
            } else {
                dsp += &COLOR_FLAG.fg_string();
            }
            dsp += FLAG;
        } else if !self.is_revealed {
            if self.is_press {
                dsp += &HOVER_COLOR.fg_string();
            } else {
                dsp += &BLANK_COLOR.fg_string();
            }
            dsp += self.wall;
        } else if self.has_mine {
            if self.is_exploded {
                dsp += &color::AnsiValue(0).fg_string();
                dsp += &color::AnsiValue(1).bg_string();
                dsp += &termion::style::Bold.to_string();
            } else {
                dsp += &color::AnsiValue(1).fg_string();
            }
            dsp += BOMB;
        } else {
            dsp += &FILLED_COLOR.fg_string();
            if self.neighboor != 0 {
                dsp += self.neighboor.to_string().as_str();
            } else {
                dsp += " ";
            }
        }
        dsp += &termion::style::Reset.to_string();
        write!(
            f,
            "{}{}",
            termion::cursor::Goto(self.pos.x, self.pos.y),
            dsp
        )
    }
}
