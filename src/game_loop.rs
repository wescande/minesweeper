use crate::{
    fsm::State,
    result::{Error, Result},
    settings::Settings,
    Opts,
};
use signal_hook::{consts::SIGWINCH, iterator::Signals};
use std::{io, io::Write};
use termion::{
    color, event::*, event_sig::EventsSig, input::MouseTerminal, raw::IntoRawMode,
    screen::AlternateScreen,
};

pub(super) fn internal_loop(opts: Opts) -> Result<()> {
    trace!("Create initial settings");
    let mut settings = Settings::new_empty()?;
    settings.parse_opts(&opts)?;
    trace!("Init game state machine");
    let mut game_fsm = State::Settings(settings);

    if opts.skip_settings {
        game_fsm = game_fsm.next()?;
    }
    trace!("Start alternate screen");
    let mut stdout = AlternateScreen::from(MouseTerminal::from(io::stdout().into_raw_mode()?));
    let events = EventsSig::new(io::stdin(), io::stdout(), Signals::new(&[SIGWINCH]).ok());
    game_fsm.full_display()?;
    stdout.flush()?;
    for evt in events {
        let ret = match evt? {
            Event::Key(Key::Char('q')) | Event::Key(Key::Esc) => break,
            Event::Resize(x, y) => game_fsm.event(Event::Resize(x, y)),
            e => game_fsm.event(e),
        };
        match ret {
            Err(Error::TerminalTooSmall) => {
                let mut s = String::new();
                s += &termion::clear::All.to_string();
                s += &color::AnsiValue(1).fg_string();
                let label: [&str; 3] = ["TERMINAL", "TOO", "SMALL"];
                let term_size = game_fsm.get_term_size();
                let mut y = term_size.y / 2 - 1;
                for i in label.iter() {
                    s += &termion::cursor::Goto((term_size.x - i.len() as u16) / 2, y).to_string();
                    s += i;
                    y += 1;
                }
                s += &termion::style::Reset.to_string();
                print!("{}", s);
            }
            Ok(true) => {
                game_fsm = game_fsm.next()?;
                game_fsm.full_display()?;
            }
            Err(x) => return Err(x),
            _ => (),
        }
        stdout.flush()?;
    }
    debug!("Return user quit now");
    Err(Error::UserQuit)
}
