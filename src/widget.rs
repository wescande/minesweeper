#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

use crate::result::Result;
use crate::utils::*;
// use std::cell::RefCell;
use std::fmt;
// use std::rc::Rc;
use termion::{color, cursor, style};

use crate::settings::{BLANK_COLOR, FILLED_COLOR};

#[derive(Debug)]
pub(crate) enum Widgets {
    Slider(Slider),
    Button(Button),
    Checkbox(Checkbox),
}

// macro_rules! widget_dispatch {
//     ($widg:tt, $closure:expr) => {
//         match $widg {
//             Widgets::Slider(slid) => $closure(slid),
//             Widgets::Button(btn) => $closure(btn),
//             Widgets::Checkbox(check) => $closure(check),
//         }
//     };
// }

impl fmt::Display for Widgets {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // widget_dispatch!(self, |item| write!(f, "{}", item))
        match self {
            Widgets::Slider(slid) => write!(f, "{}", slid),
            Widgets::Button(btn) => write!(f, "{}", btn),
            Widgets::Checkbox(check) => write!(f, "{}", check),
        }
    }
}
impl Widgets {
    pub fn is_hit(&self, x: u16, y: u16) -> bool {
        // widget_dispatch!(self, move |item| item.is_hit(x, y))
        match self {
            Widgets::Slider(slid) => slid.is_hit(x, y),
            Widgets::Button(btn) => btn.is_hit(x, y),
            Widgets::Checkbox(check) => check.is_hit(x, y),
        }
    }
    pub fn update(&mut self, x: u16, y: u16) {
        match self {
            Widgets::Slider(slid) => slid.update(x, y),
            Widgets::Button(btn) => btn.update(x, y),
            Widgets::Checkbox(check) => check.update(x, y),
        }
    }
    pub fn get_pos(&self) -> Vec2 {
        match self {
            Widgets::Slider(slid) => slid.pos.clone(),
            Widgets::Button(btn) => btn.pos.clone(),
            Widgets::Checkbox(check) => check.pos.clone(),
        }
    }
    pub fn set_pos(&mut self, pos: Vec2) {
        match self {
            Widgets::Slider(slid) => slid.pos = pos,
            Widgets::Button(btn) => btn.pos = pos,
            Widgets::Checkbox(check) => check.pos = pos,
        }
    }
    pub fn set_width(&mut self, width: usize) {
        match self {
            Widgets::Slider(slid) => slid.set_width(width),
            Widgets::Button(btn) => btn.set_width(width),
            Widgets::Checkbox(check) => check.set_width(width),
        }
    }
    pub fn get_value(&self) -> u16 {
        match self {
            Widgets::Slider(slid) => slid.value,
            Widgets::Button(btn) => btn.is_pressed as u16,
            Widgets::Checkbox(check) => check.is_checked as u16,
        }
    }
    pub fn set_value(&mut self, value: u16) {
        match self {
            Widgets::Slider(slid) => slid.value = value,
            Widgets::Button(btn) => btn.is_pressed = value != 0,
            Widgets::Checkbox(check) => check.is_checked = value != 0,
        }
    }
    pub fn select(&mut self) {
        match self {
            Widgets::Checkbox(check) => check.is_checked = !check.is_checked,
            _ => (),
        }
        print!("{}", self);
    }
    pub fn add(&mut self, val: u16) {
        match self {
            Widgets::Slider(slid) => {
                slid.value += val;
                if slid.value > slid.max {
                    slid.value = slid.max;
                }
            }
            _ => (),
        }
        print!("{}", self);
    }
    pub fn sub(&mut self, val: u16) {
        match self {
            Widgets::Slider(slid) => {
                slid.value -= val;
                if slid.value < slid.min {
                    slid.value = slid.min;
                }
            }
            _ => (),
        }
        print!("{}", self);
    }
    pub fn new_slider(pos: &Vec2, width: u16, max: u16, min: u16, value: u16) -> Result<Self> {
        Ok(Widgets::Slider(Slider::new(pos, width, max, min, value)?))
    }
    pub fn new_button(pos: &Vec2, width: usize, label: String) -> Result<Self> {
        Ok(Widgets::Button(Button::new(pos, width, label)))
    }
    pub fn new_checkbox(pos: &Vec2, width: usize, label: String, is_checked: bool) -> Result<Self> {
        Ok(Widgets::Checkbox(Checkbox::new(
            pos, width, label, is_checked,
        )))
    }
    pub fn set_max(&mut self, max: u16) {
        match self {
            Widgets::Slider(slid) => slid.set_max(max),
            _ => (),
        }
    }
}

#[derive(Debug)]
pub struct Checkbox {
    pos: Vec2,
    is_checked: bool,
    label: String,
    width: usize,
}
impl Checkbox {
    pub fn new(pos: &Vec2, width: usize, label: String, is_checked: bool) -> Self {
        Self {
            pos: pos.clone(),
            width: width - 3,
            label,
            is_checked,
        }
    }
    pub fn update(&mut self, x: u16, y: u16) {
        if self.is_hit(x, y) {
            self.is_checked = !self.is_checked;
        }
    }
    pub fn set_width(&mut self, width: usize) {
        self.width = width - 3;
    }
    pub fn is_hit(&self, x: u16, y: u16) -> bool {
        y == self.pos.y && x >= self.pos.x && x <= self.pos.x + 3 + self.width as u16
    }
}
impl fmt::Display for Checkbox {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{pos}{RAZ}{color}{:<w$.w$}{RAZ}[{color}{}{RAZ}]",
            self.label,
            if self.is_checked { 'x' } else { ' ' },
            color = FILLED_COLOR.fg_string(),
            pos = cursor::Goto(self.pos.x, self.pos.y),
            RAZ = style::Reset,
            w = self.width,
        )
    }
}

#[derive(Debug)]
pub struct Slider {
    // label: String,
    pos: Vec2,
    width: u16,
    max: u16,
    min: u16,
    pub value: u16,
    text_display: bool,
    border_display: bool,
}

impl Slider {
    pub fn new(pos: &Vec2, mut width: u16, max: u16, min: u16, value: u16) -> Result<Self> {
        let text_display = width > 12;
        if text_display {
            width -= 6;
        }
        let border_display = width > 3;
        if border_display {
            width -= 2;
        }
        Ok(Self {
            pos: pos.clone(),
            width,
            max,
            min,
            value,
            text_display,
            border_display,
        })
    }
    pub fn update(&mut self, x: u16, _y: u16) {
        let val = if x <= self.pos.x {
            self.min
        } else if x >= self.pos.x + self.width {
            self.max
        } else {
            self.min + (self.max - self.min) * (x - self.pos.x) / self.width
        };
        if val != self.value {
            self.value = val;
            print!("{}", self);
        }
    }
    pub fn set_width(&mut self, mut width: usize) {
        let text_display = width > 12;
        if text_display {
            width -= 6;
        }
        let border_display = width > 3;
        if border_display {
            width -= 2;
        }
        self.width = width as u16;
        self.text_display = text_display;
        self.border_display = border_display;
    }
    pub fn set_max(&mut self, max: u16) {
        self.max = max;
        if self.value > max {
            self.value = max;
        }
    }

    pub fn is_hit(&self, x: u16, y: u16) -> bool {
        x >= self.pos.x && x <= self.pos.x + self.width as u16 && y == self.pos.y
    }
}

impl fmt::Display for Slider {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let filled = ((self.value - self.min) * self.width / (self.max - self.min)) as usize;
        let blank = self.width as usize - filled;
        write!(
            f,
            "{pos}{RAZ}{}{color_filled}{e:■^f$}{color_blank}{e:·^b$}{RAZ}{}{}{RAZ}",
            if self.border_display { "[" } else { "" },
            if self.border_display { "]" } else { "" },
            if !self.text_display {
                "".to_string()
            } else {
                format!(
                    " {color_filled}{:3.3}{color_blank}/{:<3.3}",
                    self.value,
                    self.max,
                    color_filled = FILLED_COLOR.fg_string(),
                    color_blank = BLANK_COLOR.fg_string(),
                )
            },
            pos = cursor::Goto(self.pos.x, self.pos.y),
            RAZ = style::Reset,
            color_filled = FILLED_COLOR.fg_string(),
            color_blank = BLANK_COLOR.fg_string(),
            f = filled,
            b = blank,
            e = "",
        )
    }
}

#[derive(Debug)]
pub struct Button {
    pub pos: Vec2,
    pub width: usize,
    pub label: String,
    pub is_pressed: bool,
}

impl Button {
    pub fn new(pos: &Vec2, width: usize, label: String) -> Self {
        Self {
            pos: pos.clone(),
            width,
            label,
            is_pressed: false,
        }
    }
    pub fn is_hit(&self, x: u16, y: u16) -> bool {
        x >= self.pos.x && x <= self.pos.x + self.width as u16 && y == self.pos.y
    }
    pub fn set_width(&mut self, width: usize) {
        self.width = width;
    }
    pub fn update(&mut self, x: u16, y: u16) {
        self.is_pressed = self.is_hit(x, y);
    }
}

impl fmt::Display for Button {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{pos}{RAZ}{}{}{}{: ^w$}{RAZ}",
            style::Bold,
            color::AnsiValue(if self.is_pressed { 8 } else { 6 }).fg_string(),
            color::AnsiValue(if self.is_pressed { 6 } else { 8 }).bg_string(),
            self.label,
            pos = cursor::Goto(self.pos.x, self.pos.y),
            RAZ = style::Reset,
            w = self.width,
        )
    }
}

#[derive(Debug)]
pub struct Frame {
    pub size: Vec2,
    pub pos: Vec2,
    pub label: String,
}

impl fmt::Display for Frame {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{RAZ}{}╔{e:═<w$}╗{}║{fg}{: ^w$.w$}{RAZ}║{}{}╚{e:═<w$}╝",
            cursor::Goto(self.pos.x, self.pos.y),
            cursor::Goto(self.pos.x, self.pos.y + 1),
            self.label,
            (2..self.size.y)
                .map(|i| format!(
                    "{}║{e: ^w$}║",
                    cursor::Goto(self.pos.x, self.pos.y + i),
                    e = "",
                    w = self.size.x as usize
                ))
                .collect::<String>(),
            cursor::Goto(self.pos.x, self.pos.y + self.size.y),
            fg = color::AnsiValue(6).fg_string(),
            RAZ = style::Reset,
            e = "",
            w = self.size.x as usize
        )
    }
}

impl Frame {
    pub fn new(size: &Vec2, pos: &Vec2, label: String) -> Self {
        // let size = size.clone() - 2;
        Self {
            size: size.clone(),
            pos: pos.clone(),
            label,
        }
    }
    pub fn internal_width(&self) -> u16 {
        self.size.x - 2
    }
}
