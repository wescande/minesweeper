use crate::{
    result::{Error, Result},
    utils::*,
};

use std::str::FromStr;

pub(crate) struct Difficulty {
    pub size: Vec2,
    pub bomb: u16,
}

impl Difficulty {
    pub fn get(difficulty_level: &DifficultyLevel) -> Result<&'static Self> {
        DEFAULT_SETTINGS
            .get(difficulty_level.get_index())
            .ok_or(Error::Failed)
    }
}

#[derive(Debug)]
pub(crate) enum DifficultyLevel {
    Easy,
    Medium,
    Expert,
    // Custom,
}

impl FromStr for DifficultyLevel {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        Ok(match s.to_lowercase().as_str() {
            "easy" => DifficultyLevel::Easy,
            "medium" => DifficultyLevel::Medium,
            "expert" => DifficultyLevel::Expert,
            _ => {
                return Err(Error::Msg(format!(
                    "'{}' is not valid. You should choose between: easy|medium|expert",
                    s
                )))
            }
        })
    }
}

impl DifficultyLevel {
    fn get_index(&self) -> usize {
        match self {
            DifficultyLevel::Easy => 0,
            DifficultyLevel::Medium => 1,
            DifficultyLevel::Expert => 2,
            // DifficultyLevel::Custom => 3,
        }
    }
    // fn from_index(i: usize) -> Self {
    //     match i {
    //         0 => DifficultyLevel::Easy,
    //         1 => DifficultyLevel::Medium,
    //         2 => DifficultyLevel::Expert,
    //         _ => DifficultyLevel::Custom,
    //     }
    // }
    // pub fn get_difficulty_level(size: Vec2, bomb: u16) -> Self {
    //     for (index, elem) in DEFAULT_SETTINGS.iter().enumerate() {
    //         if elem.size == size && elem.bomb == bomb {
    //             return Self::from_index(index);
    //         }
    //     }
    //     return DifficultyLevel::Custom;
    // }
}

const DEFAULT_SETTINGS: [Difficulty; 3] = [
    Difficulty {
        size: Vec2 { x: 8, y: 8 },
        bomb: 10,
    },
    Difficulty {
        size: Vec2 { x: 16, y: 16 },
        bomb: 40,
    },
    Difficulty {
        size: Vec2 { x: 30, y: 16 },
        bomb: 99,
    },
];
