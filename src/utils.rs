use std::ops;

#[derive(Debug, Default, Clone)]
pub struct Vec2 {
    pub x: u16,
    pub y: u16,
}
impl PartialEq for Vec2 {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}
impl<T> From<(T, T)> for Vec2
where
    T: Into<u16>,
{
    #[inline]
    fn from(val: (T, T)) -> Self {
        Self {
            x: val.0.into(),
            y: val.1.into(),
        }
    }
}
impl Vec2 {
    pub fn clamp_v2(&self, other: &Self) -> Self {
        Self {
            x: if self.x < other.x { self.x } else { other.x },
            y: if self.y < other.y { self.y } else { other.y },
        }
    }
    pub fn center_to(&self, other: &Self) -> Self {
        Self {
            x: (self.x - other.x) / 2,
            y: (self.y - other.y) / 2,
        }
    }
}
impl ops::AddAssign<Vec2> for Vec2 {
    fn add_assign(&mut self, rhs: Self) {
        self.add_assign(&rhs)
    }
}
impl ops::AddAssign<&Vec2> for Vec2 {
    fn add_assign(&mut self, rhs: &Self) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}
impl ops::Add<Vec2> for Vec2 {
    type Output = Self;

    fn add(self, rhs: Vec2) -> Self {
        self + &rhs
    }
}
impl ops::Add<&Vec2> for Vec2 {
    type Output = Self;

    fn add(self, rhs: &Vec2) -> Self {
        Self {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}
impl ops::Sub<&Vec2> for Vec2 {
    type Output = Self;

    fn sub(self, rhs: &Vec2) -> Self {
        Self {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}
impl ops::Add<u16> for Vec2 {
    type Output = Self;

    fn add(self, rhs: u16) -> Self {
        Self {
            x: self.x + rhs,
            y: self.y + rhs,
        }
    }
}
impl ops::Sub<u16> for Vec2 {
    type Output = Self;

    fn sub(self, rhs: u16) -> Self {
        Self {
            x: self.x - rhs,
            y: self.y - rhs,
        }
    }
}
impl ops::Mul<u16> for Vec2 {
    type Output = Self;

    fn mul(self, rhs: u16) -> Self {
        Self {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}
impl ops::Div<u16> for Vec2 {
    type Output = Self;

    fn div(self, rhs: u16) -> Self {
        Self {
            x: self.x / rhs,
            y: self.y / rhs,
        }
    }
}
