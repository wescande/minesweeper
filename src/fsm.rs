use crate::{
    cell::Cell,
    // failed::Failed,
    play::Play,
    result::Result,
    settings::Settings,
    utils::*,
};
use std::fmt;
use termion::event::Event;

pub(crate) enum State {
    Settings(Settings),
    Play(Play),
}

impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "State::{}",
            match self {
                State::Settings(_) => "Settings",
                State::Play(_) => "Play",
            }
        )
    }
}

impl State {
    pub fn get_term_size(&self) -> Vec2 {
        match self {
            State::Settings(settings) => &settings.terminal_size,
            State::Play(play) => &play.terminal_size,
        }
        .clone()
    }
    pub fn next(self) -> Result<State> {
        let s = format!("{}", self);
        let ret = match self {
            State::Settings(settings) => State::Play(settings_to_play(settings)?),
            State::Play(play) => State::Settings(play_to_settings(play)?),
        };
        debug!("Change state: [{}] => [{}]", s, ret);
        Ok(ret)
    }
    pub fn full_display(&mut self) -> Result<()> {
        match self {
            State::Settings(settings) => settings.full_display(),
            State::Play(play) => play.full_display(),
        }
        Ok(())
    }
    pub fn event(&mut self, e: Event) -> Result<bool> {
        match self {
            State::Settings(settings) => settings.event(e),
            State::Play(play) => play.event(e),
        }
    }
}

fn settings_to_play(s: Settings) -> Result<Play> {
    let board_size = Vec2 {
        x: s.x_slider.borrow().get_value(),
        y: s.y_slider.borrow().get_value(),
    };
    let user_hide_wall = s.narrow.borrow().get_value() != 0;
    let display_size = if user_hide_wall {
        board_size.clone()
    } else {
        board_size.clone() * 2
    };
    let mut p = Play {
        terminal_size: s.terminal_size,
        cells: vec![Cell::default(); (board_size.x * board_size.y) as usize],
        bomb_count: s.bomb_slider.borrow().get_value(),
        board_size,
        display_size,
        user_hide_wall,
        wall_override: user_hide_wall,
        pressed: Vec::with_capacity(9),
        ..Play::default()
    };
    p.responsive(&p.terminal_size.clone())?;
    Ok(p)
}

fn play_to_settings(play: Play) -> Result<Settings> {
    let mut s = Settings::new_empty()?;
    s.set_value(play.board_size, play.bomb_count, play.user_hide_wall)?;
    Ok(s)
}
