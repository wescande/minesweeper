#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use std::{fmt, io};

pub type Result<T> = std::result::Result<T, Error>;

pub enum Error {
    Msg(String),
    BombExplode,
    UserQuit,
    NotATTY,
    Failed,
    Restart,
    TerminalTooSmall,
    Io(io::Error),
    LogErr(log::SetLoggerError),
    // Nix(nix::Error),
}
impl From<io::Error> for Error {
    #[inline]
    fn from(io_error: io::Error) -> Self {
        Error::Io(io_error)
    }
}

impl From<log::SetLoggerError> for Error {
    #[inline]
    fn from(log_error: log::SetLoggerError) -> Self {
        Error::LogErr(log_error)
    }
}

// impl From<nix::Error> for Error {
//     #[inline]
//     fn from(nix_error: nix::Error) -> Self {
//         Error::Nix(nix_error)
//     }
// }

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}
impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use termion::{color, style};
        let descr = match self {
            Self::Msg(s) => s.to_string(),
            Self::BombExplode => "Bomb as exploded".to_string(),
            Self::UserQuit => "User quit request".to_string(),
            Self::Restart => "User restart request".to_string(),
            Self::NotATTY => "Output is not a TTY :(".to_string(),
            Self::Failed => "Unknow generic Error".to_string(),
            Self::TerminalTooSmall => "Terminal is too small".to_string(),
            Self::Io(x) => format!("IO Error: {:?}", x),
            Self::LogErr(x) => format!("Log Error: {:?}", x),
            // Self::Nix(x) => format!("Nix Error: {:?}", x),
        };
        write!(
            f,
            "{}{}{}",
            color::AnsiValue(1).fg_string(),
            descr,
            style::Reset,
        )
    }
}
