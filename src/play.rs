#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

use crate::{
    cell::Cell,
    result::{Error, Result},
    settings::BLANK_COLOR,
    utils::*,
};
use rand::seq::SliceRandom;
use std::time::Instant;
use termion::event::*;

#[derive(Debug, Default, Clone)]
pub(crate) struct Mouse {
    pub pos: Option<Vec2>,
    pub left: bool,
    pub right: bool,
    pub middle: bool,
}

#[derive(Debug, Default)]
pub(crate) struct Play {
    pub terminal_size: Vec2,
    pub board_size: Vec2,
    pub display_size: Vec2,

    pub mouse: Mouse,
    pub too_small: bool,
    /// Cell storage
    pub cells: Vec<Cell>,
    pub pressed: Vec<Vec2>,
    pub is_start: bool,
    pub is_ended: bool,
    pub is_win: bool,
    pub board_pos: Vec2,
    pub bomb_count: u16,
    pub flag_count: i16,
    pub user_hide_wall: bool,
    pub wall_override: bool,
    pub user_wall_override: bool,
    pub start_time: Option<Instant>,
    pub win_time: u64,
}

#[derive(Debug)]
pub(crate) enum MouseClick {
    Left(Vec2),
    Right(Vec2),
    Middle(Vec2), // Middle is middle click and/or both left + right
}

impl MouseClick {
    pub fn get_pos(&self) -> Vec2 {
        match self {
            MouseClick::Left(p) => p.clone(),
            MouseClick::Right(p) => p.clone(),
            MouseClick::Middle(p) => p.clone(),
        }
    }
}
impl From<&Mouse> for Option<MouseClick> {
    #[inline]
    fn from(mouse: &Mouse) -> Self {
        if let Some(pos) = mouse.pos.clone() {
            Some(if (mouse.left && mouse.right) || mouse.middle {
                MouseClick::Middle
            } else if mouse.left {
                MouseClick::Left
            } else if mouse.right {
                MouseClick::Right
            } else {
                return None;
            }(pos))
        } else {
            None
        }
    }
}

impl Play {
    pub fn cell_display(&self) -> String {
        let mut dsp: String = String::new();
        for cell in &self.cells {
            dsp += &cell.to_string();
        }
        dsp
    }
    pub fn grid_display(&self) -> String {
        let pos = &self.board_pos;
        let top = "┌".to_string() + &"─┬".repeat(self.board_size.x as usize - 1) + "─┐";
        let mid = "├".to_string() + &"─┼".repeat(self.board_size.x as usize - 1) + "─┤";
        let core = "│ ".repeat(self.board_size.x as usize) + "│";
        let bot = "└".to_string() + &"─┴".repeat(self.board_size.x as usize - 1) + "─┘";
        let mut dsp: String =
            termion::cursor::Goto(pos.x - 1, pos.y - 1).to_string() + &BLANK_COLOR.fg_string();
        dsp += &top;
        dsp += &termion::cursor::Goto(pos.x - 1, pos.y).to_string();
        dsp += &core;
        for y in 1..self.board_size.y {
            dsp += &termion::cursor::Goto(pos.x - 1, pos.y + y * 2 - 1).to_string();
            dsp += &mid;
            dsp += &termion::cursor::Goto(pos.x - 1, pos.y + y * 2).to_string();
            dsp += &core;
        }
        dsp += &termion::cursor::Goto(pos.x - 1, pos.y + self.board_size.y * 2 - 1).to_string();
        dsp += &bot;
        dsp
    }

    pub fn full_display_end(&self) {
        self.full_display();
        print!(
            "{}{}",
            termion::cursor::Goto(self.board_pos.x, self.board_pos.y + self.display_size.y)
                .to_string(),
            "Press |R| to restart |Q| to quit"
        );
    }
    pub fn full_display(&self) {
        print!(
            "{}{}",
            termion::clear::All,
            if self.wall_override {
                self.cell_display()
            } else {
                self.grid_display() + &self.cell_display()
            }
        );
        self.state_print();
    }

    fn release(&mut self, opt_btn: Option<MouseButton>, x: u16, y: u16) -> Result<()> {
        let px = Vec2 { x, y };
        self.mouse.pos = self.from_screen_to_board(&px);
        let opt_click = (&self.mouse).into();
        match opt_btn {
            Some(MouseButton::Left) => self.mouse.left = false,
            Some(MouseButton::Right) => {
                self.mouse.right = false;
                self.mouse.left = false;
            }
            _ => self.mouse = Mouse::default(),
        }
        match opt_click {
            Some(MouseClick::Right(_)) => (), // Right click is not a trigger
            Some(click) => match self.trigger(click) {
                Err(Error::BombExplode) => {
                    for cell in &mut self.cells {
                        cell.final_reveal();
                    }
                    self.full_display_end();
                    self.is_ended = true;
                }
                x => x?,
            },
            None => self.clean_hover(),
        }
        Ok(())
    }
    fn press_or_hold(&mut self, btn: MouseButton, x: u16, y: u16, is_hold: bool) -> Result<()> {
        let px = Vec2 { x, y };
        let previous = self.mouse.pos.clone();
        self.mouse.pos = self.from_screen_to_board(&px);
        if is_hold && self.mouse.pos.is_none() && !self.wall_override {
            self.mouse.pos = self.from_screen_to_board_tolerance(&px, previous);
        }

        match btn {
            MouseButton::Left => self.mouse.left = true,
            MouseButton::Right => {
                if self.mouse.right {
                    // Dismiss because hold event on right click do not spam flag
                    return Ok(());
                }
                self.mouse.right = true;
            }
            MouseButton::Middle => self.mouse.middle = true,
            _ => (),
        }
        self.hover((&self.mouse).into())?;
        Ok(())
    }

    pub fn event(&mut self, e: Event) -> Result<bool> {
        if let Event::Resize(_, _) = e {
        } else if self.too_small {
            error!("play te");
            return Err(Error::TerminalTooSmall);
        }
        match e {
            Event::Resize(x, y) => {
                let ret = self.responsive(&Vec2 { x, y });
                self.too_small = ret.is_err();
                ret?;
                if self.is_ended {
                    self.full_display_end()
                } else {
                    self.full_display()
                }
            }
            Event::Mouse(mouse) => {
                if !self.is_ended {
                    match mouse {
                        MouseEvent::Press(btn, x, y) => self.press_or_hold(btn, x, y, false)?,
                        MouseEvent::Hold(btn, x, y) => self.press_or_hold(btn, x, y, true)?,
                        MouseEvent::Release(opt_btn, x, y) => self.release(opt_btn, x, y)?,
                    }
                }
            }
            Event::Key(key) => match key {
                Key::Char('w') => {
                    self.user_wall_override = !self.user_wall_override;
                    self.responsive(&self.terminal_size.clone())?;
                    if self.is_ended {
                        self.full_display_end()
                    } else {
                        self.full_display()
                    }
                }
                Key::Char('r') => {
                    debug!("Restart ask by player");
                    return Ok(true);
                }
                _ => (),
            },
            _ => (),
        }
        self.check_win();
        self.state_print();
        Ok(false)
    }

    fn check_win(&mut self) {
        if !self.is_start || self.is_ended {
            return;
        }
        for cell in &self.cells {
            if !cell.check_win() {
                return;
            }
        }
        for cell in &mut self.cells {
            cell.final_reveal_win();
        }
        self.flag_count = self.bomb_count as i16;
        self.is_win = true;
        self.is_ended = true;
        self.win_time = self
            .start_time
            .unwrap_or(Instant::now())
            .elapsed()
            .as_secs();
        self.state_print();
        self.full_display_end();
    }

    fn state_print(&self) {
        if self.wall_override && self.board_pos.y == 1 {
            return;
        }
        let mut pos = self.board_pos.clone();
        if pos.y > 1 {
            pos.y -= 1;
        }
        print!(
            "{}{} {}{:4.4} {}",
            termion::cursor::Goto(pos.x, pos.y),
            termion::color::AnsiValue(8).fg_string(),
            crate::cell::FLAG,
            self.bomb_count as i16 - self.flag_count,
            termion::style::Reset
        );
        if self.board_size.x < 10 || self.start_time.is_none() {
            return;
        }
        print!(
            "{} {}⏲ {} {}",
            termion::cursor::Goto(pos.x + 8, pos.y),
            termion::color::AnsiValue(8).fg_string(),
            if self.is_ended {
                self.win_time
            } else {
                self.start_time
                    .unwrap_or(Instant::now())
                    .elapsed()
                    .as_secs()
            },
            termion::style::Reset
        );
    }

    fn from_screen_to_board_tolerance(
        &self,
        pos: &Vec2,
        opt_previous: Option<Vec2>,
    ) -> Option<Vec2> {
        let previous = opt_previous?;
        let pos = pos.clone() + 1;
        if pos.x < self.board_pos.x
            || pos.y < self.board_pos.y
            || pos.x > self.board_pos.x + self.display_size.x
            || pos.y > self.board_pos.y + self.display_size.y
        {
            None
        } else {
            let mut board_pos = (pos.clone() - &self.board_pos) / 2;
            let good_fit = board_pos.clone() * 2 + &self.board_pos + 1;
            if good_fit.x != pos.x && previous.x < board_pos.x {
                board_pos.x -= 1;
            }
            if good_fit.y != pos.y && previous.y < board_pos.y {
                board_pos.y -= 1;
            }
            Some(board_pos)
        }
    }
    fn from_screen_to_board(&self, pos: &Vec2) -> Option<Vec2> {
        if pos.x < self.board_pos.x
            || pos.y < self.board_pos.y
            || pos.x >= self.board_pos.x + self.display_size.x
            || pos.y >= self.board_pos.y + self.display_size.y
        {
            None
        } else if self.wall_override {
            Some(Vec2 {
                x: pos.x - self.board_pos.x,
                y: pos.y - self.board_pos.y,
            })
        } else {
            let board_pos = (pos.clone() - &self.board_pos) / 2;
            let good_fit = board_pos.clone() * 2 + &self.board_pos;
            if good_fit.x != pos.x || good_fit.y != pos.y {
                None
            } else {
                Some(board_pos)
            }
        }
    }
    pub fn responsive(&mut self, new_size: &Vec2) -> Result<()> {
        self.terminal_size = new_size.clone();
        if self.display_size.x > new_size.x
            || self.display_size.y > new_size.y
            || (!self.wall_override && self.user_wall_override)
        {
            if self.wall_override {
                Err(Error::TerminalTooSmall)
            } else {
                if self.board_size.x > new_size.x || self.board_size.y > new_size.y {
                    Err(Error::TerminalTooSmall)
                } else {
                    self.wall_override = true;
                    self.display_size = self.board_size.clone();
                    self.responsive(new_size)
                }
            }
        } else {
            if self.wall_override && !self.user_hide_wall && !self.user_wall_override {
                let tmp_size = self.board_size.clone() * 2 + 1;
                if tmp_size.x < new_size.x && tmp_size.y < new_size.y {
                    self.display_size = self.board_size.clone() * 2;
                    self.wall_override = false;
                }
            }
            self.board_pos = self.terminal_size.center_to(&self.display_size)
                + if self.wall_override { 1 } else { 2 };
            self.resize_board();
            Ok(())
        }
    }
    fn vec2_to_index(&self, pos: &Vec2) -> usize {
        (pos.x + pos.y * self.board_size.x) as usize
    }
    fn resize_board(&mut self) {
        const UNSEE: &str = "█";
        const UNSEE_NARROW: &str = "▮";
        let wall = if self.wall_override {
            UNSEE_NARROW
        } else {
            UNSEE
        };
        for x in 0..self.board_size.x {
            for y in 0..self.board_size.y {
                let pos = Vec2 { x, y };
                let index = self.vec2_to_index(&pos);
                self.cells[index].wall = wall;
                self.cells[index].pos = if self.wall_override {
                    pos.clone()
                } else {
                    pos.clone() * 2
                } + &self.board_pos;
            }
        }
    }
    fn get_neighboor(&self, pos: &Vec2) -> Vec<Vec2> {
        let mut v = Vec::with_capacity(9);
        let mut start = pos.clone();
        if start.x > 0 {
            start.x -= 1;
        }
        if start.y > 0 {
            start.y -= 1;
        }
        let end = (pos.clone() + 1).clamp_v2(&(self.board_size.clone() - 1));
        for y in start.y..=end.y {
            for x in start.x..=end.x {
                v.push(Vec2 { x, y });
            }
        }
        v
    }
    fn clean_hover(&mut self) {
        for pos in self.pressed.drain(..) {
            let index = (pos.x + pos.y * self.board_size.x) as usize;
            self.cells[index].toggle_press(false);
        }
    }
    fn hover(&mut self, click: Option<MouseClick>) -> Result<()> {
        self.clean_hover();
        if let Some(click) = click {
            match click {
                MouseClick::Right(p) => {
                    let index = self.vec2_to_index(&p);
                    self.flag_count += self.cells[index].toggle_flag();
                }
                MouseClick::Left(p) => {
                    let index = self.vec2_to_index(&p);
                    self.cells[index].toggle_press(true);
                    self.pressed.push(p);
                }
                MouseClick::Middle(p) => {
                    let neighboors = self.get_neighboor(&p);
                    for neighboor in &neighboors {
                        let index = self.vec2_to_index(&neighboor);
                        self.cells[index].toggle_press(true)
                    }
                    self.pressed = neighboors;
                }
            }
        }
        Ok(())
    }
    pub fn propagate(&mut self, cells: Vec<Vec2>) -> Result<()> {
        let mut error = false;
        for neighboor in cells {
            if let Err(_) = self.trigger(MouseClick::Left(neighboor)) {
                error = true;
            }
        }
        if error {
            Err(Error::BombExplode)
        } else {
            Ok(())
        }
    }
    pub fn trigger(&mut self, click: MouseClick) -> Result<()> {
        self.clean_hover();
        let pos = click.get_pos();
        let neighboors = self.get_neighboor(&pos);
        let square_click = match click {
            MouseClick::Middle(_) => true,
            _ => false,
        };

        if !self.is_start {
            if square_click {
                return Ok(());
            } else {
                self.instantiate_board(&neighboors, true);
                self.is_start = true;
                self.start_time = Some(Instant::now());
            }
        }
        let index = self.vec2_to_index(&pos);
        if !self.cells[index].is_revealed && !square_click {
            let count = neighboors.iter().fold(0, |sum, nei_pos| {
                let index = (nei_pos.x + nei_pos.y * self.board_size.x) as usize;
                sum + self.cells[index].has_mine as u8
            });
            self.cells[index].neighboor = count;
            if self.cells[index].reveal()? {
                self.propagate(neighboors)?;
            }
        } else if self.cells[index].is_revealed && square_click {
            let count = neighboors.iter().fold(0, |sum, nei_pos| {
                let index = (nei_pos.x + nei_pos.y * self.board_size.x) as usize;
                sum + self.cells[index].has_flag as u8
            });
            if count == self.cells[index].neighboor {
                self.propagate(neighboors)?;
            }
        }
        Ok(())
    }

    fn instantiate_board(&mut self, origin: &Vec<Vec2>, mirror_cells: bool) {
        let mut cells =
            vec![Cell::default(); (self.board_size.x * self.board_size.y) as usize - origin.len()];
        for i in 0..self.bomb_count as usize {
            cells[i].has_mine = true;
        }
        cells.shuffle(&mut rand::thread_rng());
        for neighboor in origin {
            let index = self.vec2_to_index(neighboor);
            cells.insert(index, Cell::default());
        }
        if mirror_cells {
            for i in 0..cells.len() {
                cells[i].mirror(&self.cells[i]);
            }
        }
        self.cells = cells;
    }
}
