#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

use crate::{
    difficulty::{Difficulty, DifficultyLevel},
    result::{Error, Result},
    Opts,
    {utils::*, widget::*},
};

use std::cell::RefCell;
use std::rc::Rc;

use termion::{color, event::*, terminal_size};

// const BOX_SETTINGS_SIZE: u16 = 50;
const SETTINGS_SIZE: Vec2 = Vec2 { x: 60, y: 13 };
pub const FILLED_COLOR: color::AnsiValue = color::AnsiValue(5);
pub const BLANK_COLOR: color::AnsiValue = color::AnsiValue(8);

pub(crate) struct Settings {
    pub terminal_size: Vec2,
    pub too_small: bool,
    pub slider_holded: Option<Rc<RefCell<Widgets>>>,
    pub cursor: usize,
    pub all_widgets: Vec<Rc<RefCell<Widgets>>>,
    pub frame: Frame,
    pub x_slider: Rc<RefCell<Widgets>>,
    pub y_slider: Rc<RefCell<Widgets>>,
    pub bomb_slider: Rc<RefCell<Widgets>>,
    pub narrow: Rc<RefCell<Widgets>>,
    pub btn: Button,
}

impl Settings {
    pub fn new_empty() -> Result<Self> {
        let difficulty = Difficulty::get(&DifficultyLevel::Expert)?;
        let board_size = difficulty.size.clone();
        let bomb_count = difficulty.bomb;
        let user_hide_wall = false;
        let d = &Vec2::default();
        let i = 20;
        let j = 20;
        let frame = Frame::new(d, &(d.clone() + 50), "Game settings:".to_string());
        let x_slider = Rc::new(RefCell::new(Widgets::new_slider(d, i, i, 4, board_size.x)?));
        let y_slider = Rc::new(RefCell::new(Widgets::new_slider(d, i, i, 4, board_size.y)?));
        let bomb_slider = Rc::new(RefCell::new(Widgets::new_slider(d, i, i, 0, bomb_count)?));
        let narrow = Widgets::new_checkbox(d, j, "Hide Wall".to_string(), user_hide_wall)?;
        let narrow = Rc::new(RefCell::new(narrow));
        let label = " START ";
        let btn = Button::new(d, label.len(), label.to_string());
        Ok(Self {
            terminal_size: Vec2::from(terminal_size()?),
            too_small: false,
            slider_holded: None,
            all_widgets: vec![
                x_slider.clone(),
                y_slider.clone(),
                bomb_slider.clone(),
                narrow.clone(),
            ],
            cursor: 0,
            frame,
            x_slider,
            y_slider,
            bomb_slider,
            btn,
            narrow,
        })
    }
    pub fn parse_opts(&mut self, opts: &Opts) -> Result<()> {
        let difficulty = Difficulty::get(&opts.difficulty)?;
        let board_size = Vec2 {
            x: opts.x.unwrap_or(difficulty.size.x),
            y: opts.y.unwrap_or(difficulty.size.y),
        };
        let bomb_count = opts.bomb.unwrap_or(difficulty.bomb);
        let user_hide_wall = opts.no_wall;
        self.set_value(board_size, bomb_count, user_hide_wall)
    }
    pub fn set_value(&mut self, board: Vec2, bomb: u16, wall: bool) -> Result<()> {
        self.x_slider.borrow_mut().set_value(board.x);
        self.y_slider.borrow_mut().set_value(board.y);
        self.bomb_slider.borrow_mut().set_value(bomb);
        self.narrow.borrow_mut().set_value(wall as u16);
        self.responsive(&self.terminal_size.clone())
    }
    pub fn event(&mut self, e: Event) -> Result<bool> {
        if let Event::Resize(_, _) = e {
        } else if self.too_small {
            error!("settings");
            return Err(Error::TerminalTooSmall);
        }
        match e {
            Event::Key(key) => {
                match key {
                    Key::Char('\n') => return Ok(true),
                    Key::Char(' ') => self.all_widgets[self.cursor].borrow_mut().select(),
                    Key::Right => self.all_widgets[self.cursor].borrow_mut().add(1),
                    Key::Left => self.all_widgets[self.cursor].borrow_mut().sub(1),
                    Key::Down => self.cursor = (self.cursor + 1) % self.all_widgets.len(),
                    Key::Up => {
                        self.cursor =
                            (self.cursor + self.all_widgets.len() - 1) % self.all_widgets.len()
                    }
                    _ => (),
                };
                self.max_recalc();
            }
            Event::Mouse(mouse) => match mouse {
                MouseEvent::Press(_, a, b) => {
                    for widg in &self.all_widgets {
                        if widg.borrow().is_hit(a, b) {
                            widg.borrow_mut().update(a, b);
                            self.max_recalc();
                            self.slider_holded = Some(widg.clone());
                            break;
                        }
                    }
                    if let Some(widget) = &self.slider_holded {
                        let cursor = self.cursor;
                        self.cursor = self.find_pos(&widget);
                        if cursor != self.cursor {}
                    } else {
                        if self.btn.is_hit(a, b) {
                            self.btn.update(a, b);
                        }
                    }
                }
                MouseEvent::Hold(MouseButton::Left, a, b) => {
                    if let Some(widg) = &self.slider_holded {
                        widg.borrow_mut().update(a, b);
                        self.max_recalc();
                    }
                }
                MouseEvent::Release(_, a, b) => {
                    self.slider_holded = None;
                    if self.btn.is_pressed {
                        if self.btn.is_hit(a, b) {
                            return Ok(true);
                        } else {
                            self.btn.update(a, b);
                        }
                    }
                }
                x => {
                    debug!("Unhandled Event: '{:?}'", x);
                }
            },
            Event::Resize(x, y) => {
                let ret = self.responsive(&Vec2 { x, y });
                self.too_small = ret.is_err();
                ret?;
            }
            _ => (),
        };
        self.full_display();
        Ok(false)
    }

    fn find_pos(&self, widget: &Rc<RefCell<Widgets>>) -> usize {
        for (index, widg) in self.all_widgets.iter().enumerate() {
            if Rc::ptr_eq(widg, widget) {
                return index;
            }
        }
        return 0;
    }

    fn set_pos_and_size(widget: &Rc<RefCell<Widgets>>, pos: Vec2, width: usize) {
        let mut s = widget.borrow_mut();
        s.set_pos(pos);
        s.set_width(width);
    }
    fn responsive(&mut self, new_size: &Vec2) -> Result<()> {
        self.terminal_size = new_size.clone();
        let frame_size = SETTINGS_SIZE.clamp_v2(&new_size);
        if frame_size.x < 12 || frame_size.y < SETTINGS_SIZE.y {
            error!("Too small for settings: {:?}", frame_size);
            return Err(Error::TerminalTooSmall);
        }
        debug!("Settings with {:?}", frame_size);
        self.frame.size = frame_size.clone() - 2;
        let frame_pos = new_size.center_to(&self.frame.size);
        self.frame.pos = frame_pos.clone();
        self.frame.size.y += 1;
        // self.frame.pos.y += 1;
        let delta = Vec2 { x: 0, y: 2 };
        let mut pos = Vec2 {
            x: frame_pos.x + 4,
            y: frame_pos.y + 3,
        };
        let slider_size = self.frame.internal_width() as usize - 4;
        Self::set_pos_and_size(&self.x_slider, pos.clone(), slider_size);
        pos += &delta;
        Self::set_pos_and_size(&self.y_slider, pos.clone(), slider_size);
        pos += &delta;
        Self::set_pos_and_size(&self.bomb_slider, pos.clone(), slider_size);
        pos += &delta;
        Self::set_pos_and_size(&self.narrow, pos.clone(), slider_size);
        self.btn.pos = Vec2 {
            x: frame_pos.x + (frame_size.x - self.btn.label.len() as u16) / 2,
            y: pos.y + delta.y,
        };
        self.max_recalc();
        Ok(())
    }

    fn max_recalc(&self) {
        let max_size = if self.narrow.borrow().get_value() == 1 {
            self.terminal_size.clone()
        } else {
            self.terminal_size.clone() / 2 - 1
        };

        self.x_slider.borrow_mut().set_max(max_size.x);
        self.y_slider.borrow_mut().set_max(max_size.y);
        self.bomb_slider.borrow_mut().set_max(std::cmp::min(
            self.x_slider.borrow().get_value() * self.y_slider.borrow().get_value() - 9,
            999,
        ));
    }
    fn slider_to_string(&self) -> String {
        self.all_widgets
            .iter()
            .fold(String::new(), |acc, num| acc + &num.borrow().to_string())
    }
    pub fn full_display(&self) {
        let cursor = self.all_widgets[self.cursor].borrow().get_pos();
        print!(
            "{}{}{}{}{}{}>{}",
            termion::clear::All,
            self.frame,
            self.slider_to_string(),
            self.btn,
            termion::cursor::Goto(self.frame.pos.x + 2, cursor.y),
            FILLED_COLOR.fg_string(),
            termion::style::Reset,
        );
    }
}
