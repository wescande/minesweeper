use std::panic;

use crate::CRATE_MODULE as MODULE;

fn analyze_symbol(index: i32, symbol: &backtrace::Symbol) -> Option<String> {
    let name = symbol.name()?.to_string();
    if name.len() < MODULE.len() || &name[..MODULE.len()] != MODULE {
        None
    } else {
        Some(format!(
            "\t{}: {}\n\t\t at {}:{}:{}\n",
            index,
            &name[..(name.len() - 19)],
            symbol.filename()?.file_name()?.to_str()?,
            symbol.lineno()?,
            symbol.colno()?
        ))
    }
}

fn get_custom_backtrace() -> String {
    let mut bt_msg = String::new();
    let mut index = -2;
    backtrace::trace(|frame| {
        backtrace::resolve_frame(frame, |symbol| {
            if let Some(msg) = analyze_symbol(index, symbol) {
                if index >= 0 {
                    bt_msg += &msg;
                }
                index += 1;
            }
        });
        true // continue the backtrace
    });
    bt_msg
}

pub(super) fn custom_hook(f: fn()) {
    panic::set_hook(Box::new(move |panic_info| {
        let mut msg = "Panic occurred".to_string();
        if let Some(s) = panic_info.payload().downcast_ref::<&str>() {
            msg += &format!(": {:?}", s);
        }
        if let Some(loc) = panic_info.location() {
            msg += &format!(", {}:{}:{}", loc.file(), loc.line(), loc.column());
        }
        error!("{}\n{}", msg, get_custom_backtrace());
        f();
    }));
}
