#[macro_use]
extern crate log;

use clap::Clap;
pub use result::{Error, Result};

use std::{io, io::Write};

mod cell;
mod difficulty;
mod fsm;
mod game_loop;
mod panic;
mod play;
mod result;
mod settings;
mod utils;
#[allow(dead_code)]
mod widget;

const CRATE_MODULE: &str = module_path!();

#[derive(Clap, Debug)]
#[clap(
    version = "1.0",
    author = "William E. <william.escande+gitlab@gmail.com>"
)]
pub(crate) struct Opts {
    /// Set custom log file
    #[clap(long, default_value = "latest.log")]
    logfile: String,
    /// Optional width
    #[clap(short, long)]
    bomb: Option<u16>,
    /// Optional width
    #[clap(short)]
    x: Option<u16>,
    /// Optional height
    #[clap(short)]
    y: Option<u16>,
    /// Remove wall to set display to be as small as possible
    #[clap(short, long)]
    no_wall: bool,
    /// Start without settings confirmation
    #[clap(short, long)]
    skip_settings: bool,
    /// Start without mouse support
    #[clap(short, long)]
    mouse_off: bool,
    /// Verbosity level: -v => info | -vv => debug | -vvv => trace
    #[clap(short, long, parse(from_occurrences))]
    verbose: i32,
    /// Discard all log
    #[clap(short, long)]
    quiet: bool,
    /// Difficulty level level: Easy | Medium | Expert
    #[clap(short, long, default_value = "expert")]
    difficulty: difficulty::DifficultyLevel,
}

fn set_up_logging(verbose: i32, log_file_path: &String) -> Result<()> {
    use colored::*;
    use fern::colors::{Color, ColoredLevelConfig};
    use std::time::Instant;

    let log_level = match verbose {
        0 => log::LevelFilter::Warn,
        1 => log::LevelFilter::Info,
        2 => log::LevelFilter::Debug,
        _ => log::LevelFilter::Trace,
    };
    let colors_line = ColoredLevelConfig::new()
        .debug(Color::Blue)
        .trace(Color::BrightBlack);

    let colors_level = colors_line.clone().info(Color::Green);

    let start = Instant::now();
    let logfile = std::fs::OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(log_file_path)?;

    Ok(fern::Dispatch::new()
        .level(log_level)
        .format(move |out, message, record| {
            out.finish(format_args!(
                "{date} {level}{sep}{color_line}{message}\x1B[0m",
                date = format!("{:5.5}", start.elapsed().as_millis()).bright_black(),
                level = format!("{:5.5}", colors_level.color(record.level())).bold(),
                sep = " > ".magenta(),
                color_line = format_args!(
                    "\x1B[{}m",
                    colors_line.get_color(&record.level()).to_fg_str()
                ),
                message = message,
            ));
        })
        .chain(logfile)
        .apply()?)
}

fn main() -> Result<()> {
    use std::time::SystemTime;
    let opts: Opts = Opts::parse();
    if !opts.quiet {
        set_up_logging(opts.verbose, &opts.logfile)?
    }
    trace!(
        "Started at {:?}",
        SystemTime::UNIX_EPOCH.elapsed().unwrap().as_secs(),
    );
    trace!("Opts are:\n{:#?}", opts);
    info!("Welcome in this minesweeper!");
    if !termion::is_tty(&std::io::stdout()) {
        return Err(Error::NotATTY);
    }
    print!("{}", termion::cursor::Hide);
    let finalize = || print!("{}", termion::cursor::Show);
    panic::custom_hook(finalize);
    let ret = game_loop::internal_loop(opts);
    finalize();
    debug!("Exit with {:?}", ret);
    io::stdout().flush()?;
    match ret {
        Err(Error::UserQuit) => Ok(()),
        x => x,
    }
}
